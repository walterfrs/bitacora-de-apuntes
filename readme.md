Bitácora de tutoriales y trucos de desarrollo e informática en general

#Índice de documentos


* PHP
    + Estándares de codificación PSR (PSR-0, PSR-1 y PSR-2)
* Python > Django
    + pyjobeet
        - 01: Instalación y configuración
        - 02: Descripción del proyecto
    + Instalación y uso de VirtualEnv


Walter Fabián Rodríguez Salazar
[walterud@gmail.com]
