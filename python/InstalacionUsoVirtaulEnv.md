# Instalación y uso de VirtualEnv


Virtualenv es una herramienta de Python escrito por Ian Bicking y se
utiliza para crear entornos aislados para Python en el que se pueden
instalar paquetes sin interferir con otros virtualenvs ni con los
paquetes del sistema de Python. Virtualenvwrapper es una utilidad de
línea de comandos diseñada por Doug Hellmann que mejora el flujo de
trabajo con virtualenv[^1].


## Instalación


Instalar paquete pip desde el gestor de paquetes (para Debian):

    $ sudo aptitude install python-pip

Seguido se instala el paquete `virtualenvwrapper`

    $ sudo pip install virtualenvwrapper


## Configuración


Antes de utilizarlo se debe configurar la ruta de ubicación de los
entornos virtuales y las rutas de los  comandos para trabajar, así que
se deben agregar las dos siguientes lineas en el archivo de
configuración bash `~/.bashrc` (o `~/.zshrc` si utiliza el shell zsh):

    #!sh
    export WORKON_HOME=$HOME/.virtualenvs
    source /usr/local/bin/virtualenvwrapper.sh

Recargamos el archivo de inicio

    source ~/.bashrc

o

    source ~/.zshrc

# Uso

Los comandos mas usados para trabajar con `virtualenvwrapper` son:

* `mkvirtualenv nombre`: Crea un nuevo entorno virtual en donde
    trabajar, se instala `PIP` y `Easy_install` de manera automática.
* `lsvirtualenv`: Lista todos los entornos virtuales que existen en la
    maquina (creados con `virtualenvwrapper`).
* `workon nombre`: Activar y comenzar a trabajar en el `virtualenv`.
* `cdvirtualenv`: nos lleva hacia el directorio del `virtualenv` donde
    estamos trabajando. (debemos haber activado un `virtualenv`).
* `deactivate`: Salir del `virtualenv` en el que se encuentra.
* `rmvirtualenv nombre`: Borrar el `virtualenv`.





[^1]: https://wiki.archlinux.org/index.php/Python_VirtualEnv
