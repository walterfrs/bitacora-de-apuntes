# pyJobeet 01: Introducción e instalación


Decidí aprender a programar con el framework Django, y una de las
mejores maneras es migrando una aplicación ya existente a otro
lenguaje (y en este caso a otro framework también). En este caso
escogí migrar la aplicación Jobeet incluida en el tutorial de [Symfony
1.4](http://symfony.com/legacy/doc/jobeet/1_4/es?orm=Doctrine) a
Django, la elección por esta aplicación fue debido a que aprendí a desarrollar
en Symfony gracias a este tutorial. Aunque la versión 1.4 se encuentre
obsoleta y fue reemplazada por la nueva versión rediseñada desde de
cero de Symfony (v2.*) y por el excelente [libro de Javier
Eguiluz](http://symfony.es/libro/) hay muchos conceptos teóricos que todavía
se pueden aprovechar del tutorial Jobeet y aplicarlos a otros
frameworks.

El objetivo de esta serie de tutoriales es el aprender a desarrollar
en un nuevo framework y a la vez dejar registrado mis avances sobre
este proyecto, para que otras personas se puedan beneficiar de él y
que personas más experimentadas quieran contribuir en mejorar esta
pequeña idea que estoy llevando a cabo.

Como indique arriba, el proyecto será sobre la aplicación Jobeet, la
cual pueden ver online (La versión desarrollada en Symfony) en
<http://jobeet.org>, así mismo el tutorial original en Symfony 1.4 se
encuentra en
<http://symfony.com/legacy/doc/jobeet/1_4/es?orm=Doctrine> y su código
en <http://svn.jobeet.org>. Tanto el tutorial como el aplicativo fueron desarrollados por la empresa [Sensio Labs](http://sensiolabs.com/en).

Así mismo, en algunas partes puede que realice algunas citaciones del
tutorial Symfony como también puedan ser algunas imágenes, estos
fragmentos e imágenes seguirán manteniendo los derechos de autor
correspondientes por parte de Sensio Labs, el código fuente
acompañando en este proyecto también será liberado bajo la [misma
licencia del proyecto original](http://jobeet.org/about) (MIT).

El proyecto lo estoy desarrollando tanto en Linux como en Windows, así
que trataré de orientarlo para que se pueda desarrollar en ambos
Sistemas Operativos.

Por último antes de empezar, cualquier duda o sugerencia será
bienvenida y espero que disfruten de realizar este tutorial tanto como
yo lo disfruto realizándolo.


#Instalación de python y Django

Primero que todo vamos a instalar las herramientas necesarias para
poder desarrollar en Django la instalación de este framework varia un
poco respecto de Windows a Linux, así que primero me centraré en
explicar la instalación de Windows que es un poco más larga.

## Instalación en Windows

Para instalar Django en Windows se necesitan descargar primero que
todo el intérprete de Python, ya que Django corre sobre Python, la
versión recomendable, al momento de la creación de este tutorial es la
2.7 debido a que se presentan problemas con la versión 3 de Python y
Django. La pueden descargar de la dirección:

* <http://www.python.org/ftp/python/2.7.3/python-2.7.3.msi> para Windows 32 bits
* <http://www.python.org/ftp/python/2.7.3/python-2.7.3.amd64.msi> para Windows 64 bits

Se procede a su instalación, y a continuación la vamos a definir en el
`PATH` del sistema. Para lo cual nos vamos a `Menú Inicio > Panel de
control > Sistema y Seguridad > Sistema > Configuración avanzada del
sistema` (Esto es para Windows 7/8, para Windows XP vamos a `Mi PC`
sobre el damos click derecho, vamos a `Propiedades` y después a la
pestaña `Opciones Avanzadas`). Después pulsamos el botón
`Variables de entorno...` y buscaos la Variable `PATH`, seleccionamos
y pulsamos en `Editar...` y al final de la línea agregamos:

    ;C:/Python27;C:/Python27/Tools/Scripts;C:/Python27/Scripts

Tener mucho cuidado de no borrar el texto anterior ni que queden dos caracteres `;` seguidos:

![Asignando Python al PATH](http://dl.dropbox.com/u/46708093/scriptogram/bitacora/01-IntroVariables.png)

Clik en `Aceptar` (3 veces), abrimos una consola y escribimos: `python -V` deberá aparecerles algo parecido a esto:

    C:\> python -V
    Python 2.7.3

Si es así, ya tiene Python instalado y configurado.

### PIL (Python Imaging Library)
Lo siguiente es instalar la 'Python Imaging Library' debido a que
Vamos a trabajar con imágenes y para esto Django solicita tener
instalado el paquete PIL el cual pueden descargar de
<http://www.pythonware.com/products/pil/> e instalar la versión
correspondiente a la versión de instalación de Python (2.7 en nuestro
caso).


### Django

Para instalar Django descargamos la última versión de la pagina
<https://www.djangoproject.com/download/>, procedemos a extraer los
archivos y desde una consola nos ubicamos en la carpeta extraída (En
Windows 7/8: ejecutar la consola de comandos como administrador),
seguidamente ejecutamos:

    python setup.py install

Para validar si quedo correctamente instalado, accedemos a la consola ejecutando los siguientes comandos:

    C:\>python
    Python 2.7.3 (default, Apr 10 2012, 23:31:26)
    [MSC v.1500 32 bit (Intel)] on win 32
    Type "help", "copyright", "credits" or "license" for more information.
    >>> import django
    >>> django.VERSION
    (1, 4, 2, 'final', 0)

Para salir del intérprete de Pyton pulsar `CTRL + Z + ENTER`

Ahora ya tienes instalado Django para empezar a trabajar.


## Instalación en Linux

En Linux es más sencilla la instalación debido a que en la mayoría de
distribuciones Linux (por no decir todas) tienen instalado el
intérprete Python, lo único a validar seria si se tiene instalada la
versión 2.7.* lo cual puede ser revisado a través del gestor de
paquetes de la distribución que utiliza (apt-get, aptitude, yum, yast,
pacman,....) de la misma manera revisar si se tiene el paquete pil
(Python Imaging Library) con en el mismo gestor, si no es así, instalarlo.

Para instalar Django también lo descargamos pagina
<https://www.djangoproject.com/download/>, lo extraemos,
abrimos una terminal y nos ubicamos en la ruta de la carpeta extraída,
para instalarlo se necesitan permisos de superusuario, si tienes una
distribución basada en Debian o si tienes configurado sudo ejecuta:

    sudo python setup.py install

Si no es así, ejecuta:

    su -c 'python setup.py install'

Validamos que haya quedado bien instalado Django:

    $ python
    Python 2.7.3
    [MSC v.1500 32 bit (Intel)]
    Type "help", "copyright", "credits" or "license" for more information.
    >>> import django
    >>> django.VERSION
    (1, 4, 2, 'final', 0)

Ahora ya tienes instalado Django para empezar a trabajar.
